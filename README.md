```
module.exports = {
user: 'admin',
password: 'admin',
db: 'db',
dbuser: 'dbname',
dbpassword: 'dbpassword',
login: '/adminlogin',
loginmsg: '页面不存在',
secret: 'yoursecret',
files: 'gz|mp4|sh',
monthlimit: 30,
daylimit: 2
};
```

- monthlimit 设置月度单 IP 下载限制。
- daylimit 设置每日单 IP 下载限制。
- files 设置监控后缀。

文件放在/public/download 中的，可被系统设置 files 后缀监控到。

- 请默认设置 2 的倍数限制数，系统默认每个下载是计算两次请求，所以请设置 2 的倍数。
