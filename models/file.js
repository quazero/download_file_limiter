var mongoose = require('mongoose');
var FileSchema = require('../schemas/file');
var File = mongoose.model('File', FileSchema);

module.exports = File;
