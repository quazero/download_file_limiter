var fs = require('fs');
var _ = require('lodash');
var config = require('../config/auth');
const File = require('../models/file');
exports.getadmin = async function(req, res) {
  const page = req.query.page > 0 ? req.query.page : 1;
  const perPage = req.query.counts > 0 ? req.query.counts * 1 : 50;
  const files = await File.find()
    .sort('-createAt')
    .limit(perPage)
    .skip(perPage * (page - 1));
  const count = File.countDocuments();
  res.render('admin', {
    user: req.session.user,
    files,
    title: '文件下载限制器',
    page: page,
    pages: Math.ceil(count / perPage)
  });
};
