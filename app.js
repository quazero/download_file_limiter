var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var config = require('./config/auth');
var session = require('express-session');
var flash = require('connect-flash');
var mongoose = require('mongoose');
var MongoStore = require('connect-mongo')(session);
var routes = require('./routes/index');
const File = require('./models/file');
var app = express();
const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
};
mongoose.connect(
  'mongodb://' +
    config.dbuser +
    ':' +
    config.dbpassword +
    '@127.0.0.1/' +
    config.db,
  options
);
var moment = require('moment');
// view engine setup
app.set('trust proxy', 1);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(express.json({ limit: '5mb' }));
app.use(express.urlencoded({ limit: '5mb', extended: false }));
app.use(cookieParser());
app.use(
  session({
    secret: config.secret,
    resave: true,
    saveUninitialized: false,
    key: 'hls',
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 30
    }, //30day
    store: new MongoStore({
      url:
        'mongodb://' +
        config.dbuser +
        ':' +
        config.dbpassword +
        '@127.0.0.1/' +
        config.db
    })
  })
);
const re = new RegExp('^\\/download\\/.+\\.(' + config.files + ')$', 'i');
app.use(re, async function(req, res, next) {
  let ip = req.ip.match(/\d+\.\d+\.\d+\.\d+/);
  if (!ip) {
    ip = '0.0.0.0';
  } else {
    ip = ip[0];
  }
  const now = Date.now();
  const url = req.baseUrl;
  const lastmonth = moment(now).subtract(30, 'days');
  const monthdownloads = await File.find({ ip })
    .where('createAt')
    .gt(lastmonth)
    .countDocuments();
  if (monthdownloads && monthdownloads > config.monthlimit) {
    return res.status(404).send('对不起，您下载次数超过月度限制');
  } else {
    const lastday = moment(now).subtract(1, 'days');
    const daydownloads = await File.find({ ip })
      .where('createAt')
      .gt(lastday)
      .countDocuments();
    if (daydownloads && daydownloads > config.daylimit) {
      return res.status(404).send('对不起，您下载次数超过每日限制');
    } else {
      await File.create({ ip, url, createAt: now });
      next();
    }
  }
});
app.use(express.static(path.join(__dirname, 'public')));
app.use(function(req, res, next) {
  res.locals.createPagination = function(pages, page) {
    var url = require('url'),
      qs = require('querystring'),
      params = qs.parse(url.parse(req.url).query),
      str = '',
      list_len = 2,
      total_list = list_len * 2 + 1,
      j = 1,
      pageNo = parseInt(page);
    if (pageNo >= total_list) {
      j = pageNo - list_len;
      total_list = pageNo + list_len;
      if (total_list > pages) {
        total_list = pages;
      }
    } else {
      j = 1;
      if (total_list > pages) {
        total_list = pages;
      }
    }
    params.page = 0;
    for (j; j <= total_list; j++) {
      params.page = j;
      clas = pageNo == j ? 'active' : 'no';
      str +=
        '<li class="' +
        clas +
        '"><a href="?' +
        qs.stringify(params) +
        '">' +
        j +
        '</a></li>';
    }
    return str;
  };
  next();
});
app.use(flash());
// app.use(function (req, res, next) {
//   res.setTimeout(480000, function () { // 4 minute timeout adjust for larger uploads
//     console.log('Request has timed out.');
//     res.send(408);
//   });

//   next();
// });
routes(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
module.exports = app;
