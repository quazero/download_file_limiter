var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var FileSchema = new Schema({
  ip: String,
  url: String,
  createAt: {
    type: Date
  }
});
FileSchema.pre('save', function(next) {
  if (!this.createAt) {
    this.createAt = Date.now();
  }
  next();
});
FileSchema.index({ url: 1, ip: 1 });
FileSchema.index({ createAt: -1, ip: 1 });
module.exports = FileSchema;
