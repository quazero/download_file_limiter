var auth = require('../config/auth');
var Admincontroller = require('../controller/admin');
var config = require('../config/auth');
var multer = require('multer');
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './movies');
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});
var upload = multer({
  storage: storage,
  fileFilter: function(req, file, cb) {
    var fileFormat = file.originalname.toLowerCase().split('.');
    if (
      fileFormat[fileFormat.length - 1] !== 'mkv' &&
      fileFormat[fileFormat.length - 1] !== 'mp4' &&
      fileFormat[fileFormat.length - 1] !== 'avi' &&
      fileFormat[fileFormat.length - 1] !== 'rmvb' &&
      fileFormat[fileFormat.length - 1] !== 'rm' &&
      fileFormat[fileFormat.length - 1] !== 'flv'
    ) {
      cb(null, false);
    } else {
      cb(null, true);
    }
  }
});
module.exports = function(app) {
  app.get(config.login, checkNotLogin, function(req, res, next) {
    res.render('hlsserver', {
      title: '云转码切片服务平台'
    });
  });
  app.post(config.login, checkNotLogin, function(req, res) {
    var user = req.body.user;
    var password = req.body.password;
    if (user == auth.user && password == auth.password) {
      req.session.user = user;
      res.redirect('/admin');
    } else {
      res.redirect('https://baidu.com');
    }
  });
  app.get('/hlslogout', checkLogin, function(req, res) {
    req.session.user = null;
    res.redirect(config.login);
  });
  app.get('/admin', checkLogin, Admincontroller.getadmin);

  function checkLogin(req, res, next) {
    if (!req.session.user) {
      return res.status(404).send(config.loginmsg);
    }
    next();
  }
  function checkNotLogin(req, res, next) {
    if (req.session.user) {
      return res.redirect('/admin');
    }
    next();
  }
};
