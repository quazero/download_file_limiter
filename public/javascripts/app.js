;(function($, document, window) {
  $(document).ready(function() {
    // Cloning main navigation for mobile menu
    $(".mobile-navigation").append($(".main-navigation .menu").clone())

    // Mobile menu toggle
    $(".menu-toggle").click(function() {
      $(".mobile-navigation").slideToggle()
    })
    $(".search-form button").click(function() {
      $(this).toggleClass("active")
      var $parent = $(this).parent(".search-form")

      $parent
        .find("input")
        .toggleClass("active")
        .focus()
    })

    $(".slider").flexslider({
      controlNav: false,
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>'
    })
    if ($(".map").length) {
      $(".map").gmap3(
        {
          map: {
            options: {
              maxZoom: 14
            }
          },
          marker: {
            address: "40 Sibley St, Detroit"
          }
        },
        "autofit"
      )
    }
    $("#selectcategory").change(function(e) {
      e.preventDefault()
      var value = $(this).val()
      if (value == "movies") {
        window.location = "/movies"
      } else {
        window.location = "/category/" + value
      }
    })
    $(".search-form").submit(function(e) {
      e.preventDefault()
      var val = $(".searchq").val()
      if (val.length > 0) {
        window.location = "/search?q=" + val
      }
    })
  })

  $(window).load(function() {})
})(jQuery, document, window)
