module.exports = {
  apps: [
    {
      name: "express-ffmpeg-vip",
      script: "./bin/www",
      instances: 0,
      env: {
        PORT: 3030,
        NODE_ENV: "development"
      },
      env_uat: {
        PORT: 3030,
        NODE_ENV: "uat"
      },
      env_production: {
        PORT: 3000,
        NODE_ENV: "production"
      }
    }
  ]
}
